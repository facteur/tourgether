import os

TOURGETHER_ENVIRONMENT = os.environ.get('TOURGETHER_ENVIRONMENT')

if TOURGETHER_ENVIRONMENT == 'production':
    from .production import *
elif TOURGETHER_ENVIRONMENT == 'staging':
    from .staging import *
else:
    from default import *
