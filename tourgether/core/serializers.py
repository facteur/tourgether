from rest_framework import serializers

from tourgether.core.models import TouringUser


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TouringUser
        fields = ('id', 'url', 'username', 'email')

