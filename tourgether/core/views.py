from rest_framework import viewsets

from tourgether.core.models import TouringUser

from tourgether.core.serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = TouringUser.objects.all()
    serializer_class = UserSerializer
