from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

from rest_framework import routers

from tourgether.core import views as core_views
from touring import views as touring_views

router = routers.DefaultRouter()
router.register(r'users', core_views.UserViewSet)
router.register(r'tours_geometry', touring_views.TourGeometryViewSet)
router.register(r'tours', touring_views.TourViewSet)

urlpatterns = patterns('',
    # Django rest framework urls
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^admin/', include(admin.site.urls))
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
