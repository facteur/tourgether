from rest_framework import serializers

from tourgether.core.serializers import UserSerializer

from touring.models import Tour, TourGeometry
from touring.fields import PointsField, GeometryField


class TourGeometrySerializer(serializers.HyperlinkedModelSerializer):
    points = PointsField()

    class Meta:
        model = TourGeometry
	fields = ('id', 'url', 'points')


class TourSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.Field(source='id')
    creator = serializers.HyperlinkedRelatedField(many=False, view_name='touringuser-detail')
    geometry = GeometryField()

    class Meta:
        model = Tour
