from django.db import models
from django.contrib.gis.db import models as geomodels 
from django.conf import settings
from django.utils.formats import get_format


RECORD_SOURCE_CHOICES = (
    ('Android', 'Android'),
    ('iPhone', 'iPhone')
)

STATUS_CHOICES = (
    ('PUBLIC', 'Public'),
    ('PRIVATE', 'Private')
)

SPORT_CHOICES = (
    ('mtb', 'Moutain bike'),
    ('touringbicycle', 'Touring bicycle'),
    ('hike', 'Hike'),
    ('moutaineering', 'Moutaineering'),
    ('sled', 'Sled'),
    ('racebike', 'Race bike'),
    ('jogging', 'Jogging'),
)


class TourGeometry(models.Model):
    points = geomodels.MultiPointField(dim=3, srid=4326, geography=True, null=True)
    objects = geomodels.GeoManager()


class Tour(models.Model):
    name = models.CharField(max_length=1024, null=True)
    text = models.TextField(null=True)
    hasImage = models.BooleanField(default=False)
    record_source = models.CharField(max_length=16, choices=RECORD_SOURCE_CHOICES, null=True)
    status = models.CharField(max_length=8, choices=STATUS_CHOICES, default='PUBLIC')
    sport = models.CharField(max_length=16, null=True)

    geometry = models.ForeignKey(TourGeometry, null=True)

    # Altitudes related fields
    altDown = models.DecimalField(max_digits=32, decimal_places=16)
    altUp = models.DecimalField(max_digits=32, decimal_places=16)
    altDiff = models.DecimalField(max_digits=32, decimal_places=16)

    # Time and space related fields
    distance = models.DecimalField(max_digits=32, decimal_places=16)
    duration = models.PositiveIntegerField()

    # Contextual fields
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    createdAt = models.DateTimeField(auto_now=True, auto_now_add=True)
    changedAt = models.DateTimeField(auto_now=True)



