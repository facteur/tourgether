import json

from django.contrib.gis.geos import Point, MultiPoint

from rest_framework import serializers
from rest_framework.reverse import reverse

from touring.models import TourGeometry


class PointsField(serializers.WritableField):
    def to_native(self, obj):
        return [{'x': p.x, 'y': p.y, 'z': p.z} for p in obj]

    def from_native(self, data):
	if isinstance(data, (str, unicode)):
		data = json.loads(data)
	points = MultiPoint([Point(p['x'], p['y'], p['z']) for p in data])
	return points

class GeometryField(serializers.WritableField):
    def to_native(self, obj):
	# FIXME UGLY HACK. But django changes the way to access url all the time
	# Please fix this. Quick.
	obj_url = 'http://tourgether.softwarehater.com' + reverse('tourgeometry-detail', args=[str(obj.id)])

	return {
		'id': obj.id,
		'url': obj_url,
		'points': [{'x': p.x, 'y': p.y, 'z': p.z} for p in obj.points]
	}

    def from_native(self, data):
	if isinstance(data, (str, unicode)):
	    data = json.loads(data)
	points = MultiPoint([Point(p['x'], p['y'], p['z']) for p in data['points']])
	geometry = TourGeometry.objects.create(points=points)
	return geometry
