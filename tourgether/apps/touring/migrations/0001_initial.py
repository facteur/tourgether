# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TourGeometry'
        db.create_table(u'touring_tourgeometry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('points', self.gf('django.contrib.gis.db.models.fields.MultiPointField')(dim=3, null=True)),
        ))
        db.send_create_signal(u'touring', ['TourGeometry'])

        # Adding model 'Tour'
        db.create_table(u'touring_tour', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=1024, null=True)),
            ('text', self.gf('django.db.models.fields.TextField')(null=True)),
            ('hasImage', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('record_source', self.gf('django.db.models.fields.CharField')(max_length=16, null=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='PUBLIC', max_length=8)),
            ('sport', self.gf('django.db.models.fields.CharField')(max_length=16, null=True)),
            ('geometry', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['touring.TourGeometry'], null=True)),
            ('altDown', self.gf('django.db.models.fields.DecimalField')(max_digits=32, decimal_places=16)),
            ('altUp', self.gf('django.db.models.fields.DecimalField')(max_digits=32, decimal_places=16)),
            ('altDiff', self.gf('django.db.models.fields.DecimalField')(max_digits=32, decimal_places=16)),
            ('distance', self.gf('django.db.models.fields.DecimalField')(max_digits=32, decimal_places=16)),
            ('duration', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.TouringUser'], null=True)),
            ('createdAt', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, auto_now_add=True, blank=True)),
            ('changedAt', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'touring', ['Tour'])


    def backwards(self, orm):
        # Deleting model 'TourGeometry'
        db.delete_table(u'touring_tourgeometry')

        # Deleting model 'Tour'
        db.delete_table(u'touring_tour')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'core.touringuser': {
            'Meta': {'object_name': 'TouringUser'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'touring.tour': {
            'Meta': {'object_name': 'Tour'},
            'altDiff': ('django.db.models.fields.DecimalField', [], {'max_digits': '32', 'decimal_places': '16'}),
            'altDown': ('django.db.models.fields.DecimalField', [], {'max_digits': '32', 'decimal_places': '16'}),
            'altUp': ('django.db.models.fields.DecimalField', [], {'max_digits': '32', 'decimal_places': '16'}),
            'changedAt': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'createdAt': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.TouringUser']", 'null': 'True'}),
            'distance': ('django.db.models.fields.DecimalField', [], {'max_digits': '32', 'decimal_places': '16'}),
            'duration': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'geometry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['touring.TourGeometry']", 'null': 'True'}),
            'hasImage': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True'}),
            'record_source': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'sport': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'PUBLIC'", 'max_length': '8'}),
            'text': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'touring.tourgeometry': {
            'Meta': {'object_name': 'TourGeometry'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'points': ('django.contrib.gis.db.models.fields.MultiPointField', [], {'dim': '3', 'null': 'True'})
        }
    }

    complete_apps = ['touring']