from django.contrib.gis.geos import *

from rest_framework import viewsets, filters
from rest_framework.decorators import action

from touring.models import (
    Tour,
    TourGeometry
)

from touring.serializers import (
    TourSerializer,
    TourGeometrySerializer
)


class TourGeometryViewSet(viewsets.ModelViewSet):
    queryset = TourGeometry.objects.all()
    serializer_class = TourGeometrySerializer


class TourViewSet(viewsets.ModelViewSet):
    model = Tour
    serializer_class = TourSerializer

    def get_queryset(self):
        queryset = Tour.objects.all()

        # Activate a query ?sport filter on Tour resources
        sport = self.request.QUERY_PARAMS.get('sport')
        if sport is not None:
            queryset = queryset.filter(sport=sport)

        # Activate a query ?username filter on Tour resources
        username = self.request.QUERY_PARAMS.get('username')
        if username is not None:
            queryset = queryset.filter(creator__username=username)
        
        x = self.request.QUERY_PARAMS.get('x')
        y = self.request.QUERY_PARAMS.get('y')
        radius = self.request.QUERY_PARAMS.get('radius')
        if x and y and radius:
            seeked_point = Point(float(x), float(y), srid=4326)
            points = TourGeometry.objects.filter(points__distance_lte=(seeked_point, int(radius)))
            queryset = queryset.filter(geometry__in=points)

        return queryset

