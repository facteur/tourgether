import json
import datetime

from dateutil import parser

from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point, MultiPoint
from django.db.utils import IntegrityError

from tourgether.core.models import TouringUser

from touring.models import (
    Tour,
    TourGeometry,
)


class Command(BaseCommand):
    args = '<data_file.json data_file.json ...>'
    help = 'Import tours from a json data file'

    def extract_geometry(self, geometry_data):
        points = []

        for point in sorted(geometry_data, key=lambda item: item['time']):
	    x = point.get('x')
	    y = point.get('y')
	    z = point.get('z', 0.0)
            points.append(Point(x, y, z))

        mp = MultiPoint(*points)
	geometry = TourGeometry(points=mp)
	if all(p.z is not None for p in geometry.points):
		geometry.save()

        return geometry

    def handle(self, *args, **options):
        tours = []

        for filepath in args:
            tours_data = json.load(open(filepath, 'r'))
            print "--> Extracting {} data".format(filepath)

            for tour_data in tours_data:
                # Get or create the tour creator user
                creator_username = tour_data.get('creator')
                creator, _ = TouringUser.objects.get_or_create(username=creator_username, password='ChangeMe!')

                # If tour content is not left null,
                # create an entry for it
                tour_content = tour_data.get('content')
                if tour_content is not None:
                    tour_text = tour_content.get('text')
                    tour_has_image = tour_content.get('hasImage')

                # If a tour geometry was supplied,
                # then create the proper entries according
                # to it
                tour_geometry = tour_data.get('geometry')
                if tour_geometry is not None:
                    geometry = self.extract_geometry(tour_geometry)

                tours.append(Tour(
                    name=tour_data.get('name'),
                    text=tour_text,
                    hasImage=tour_has_image,
                    record_source=tour_data.get('recordSource'),
                    status=tour_data.get('status'),
                    sport=tour_data.get('sport'),
                    geometry=geometry,
                    altDown=tour_data['altDown'],
                    altUp=tour_data['altUp'],
                    altDiff=tour_data['altDiff'],
                    distance=tour_data['distance'],
                    duration=tour_data['duration'],
                    creator=creator,
                    createdAt=tour_data['createdAt'],
                    changedAt=tour_data['changedAt']
                ))

        print "<-- data extracted"
        print "\n--> Proceeding to data insertion into database"
        Tour.objects.bulk_create(tours)
        print "<-- All data have been inserted into database"
